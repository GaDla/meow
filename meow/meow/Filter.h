#ifndef FILTER_H
#define FILTER_H

#include "precipitation_subscription.h"

precipitation_subscription** filter(precipitation_subscription* array[], int size, bool (*check)(precipitation_subscription* element), int& result_size);

bool check_precipitation_rain(precipitation_subscription* element);

bool check_precipitation_rainfall(precipitation_subscription* element);

#endif

