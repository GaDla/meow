#ifndef FILE_READER_H
#define FILE_READER_H

#include "Precipitation_subscription.h"

void read(const char* file_name, precipitation_subscription* array[], int& size);

#endif