#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>
using namespace std;

date convert(char* str)
{
    date result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.day = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.month = atoi(str_number);
    return result;
}

void read(const char* file_name, precipitation_subscription* array[], int& size)
{
    ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            precipitation_subscription* item = new precipitation_subscription;
            file >> tmp_buffer;
            item->day_month = convert(tmp_buffer);
            file >> item->rainfall;
            file.read(tmp_buffer, 1); // ������ ������� ������� �������
            file.getline(item->precipitation_name, MAX_STRING_SIZE);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}