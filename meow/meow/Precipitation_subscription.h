#ifndef Precipitation_subscription_H
#define Precipitation_subscription_H

#include "constants.h"

struct date
{
    int day; 
    int month;
};

struct precipitation_subscription
{
    date day_month;
    double rainfall;
    char precipitation_name[MAX_STRING_SIZE];
};

#endif
