﻿#include <iostream>
#include <iomanip>

using namespace std;

#include "Precipitation_subscription.h"
#include "file_reader.h"
#include "constants.h"
#include "Filter.h"
void output(precipitation_subscription* subscriptions)
{
    //День
    cout << "День................: ";
    cout << setw(2) << setfill('0') << subscriptions->day_month.day << '\n';
    //Месяц
    cout << "Месяц...............: ";
    cout << setw(2) << subscriptions->day_month.month << '\n';
    //Кол-во осадков
    cout << "Кол-во осадков......: ";
    cout << setw(3) << subscriptions->rainfall << '\n';
    //Вид осадков
    cout << "Вид осадков.........: ";
    cout << subscriptions->precipitation_name << '\n';
    cout << '\n';
}

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "Лабораторная работа №9. GIT\n";
    cout << "Вариант №3. Осадки\n";
    cout << "Автор: Данила Гамзюк\n";
    cout << "Группа: 12\n\n";
    precipitation_subscription* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    //int meow;
    //cin >> meow;
    try
    {
        read("data.txt", subscriptions, size);
        cout << "***** Осадки *****\n\n";
        for (int i = 0; i < size; i++)
        {
            output(subscriptions[i]);
        }
        bool (*check_function)(precipitation_subscription*) = NULL; // check_function - это указатель на функцию, возвращающую значение типа bool,
        // и принимающую в качестве параметра значение типа book_subscription*
        cout << "\nВыберите способ фильтрации или обработки данных:\n";
        cout << "1 - Дни в которые шёл дождь\n";
        cout << "2 - Дни в которые осадки < 1.5\n";
        cout << "\nВведите номер выбранного пункта: ";
        int item;
        cin >> item;
        cout << '\n';
        switch (item)
        {
        case 1:
            check_function = check_precipitation_rain; // присваиваем в указатель на функцию соответствующую функцию
            cout << "***** Дни в которые шёл дождь *****\n\n";
            break;
        case 2:
            check_function = check_precipitation_rainfall; // присваиваем в указатель на функцию соответствующую функцию
            cout << "***** Дни в которые осадки < 1.5 *****\n\n";
            break;
        default:
            throw "Некорректный номер пункта";
        }
        if (check_function)
        {
            int new_size;
            precipitation_subscription** filtered = filter(subscriptions, size, check_function, new_size);
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}