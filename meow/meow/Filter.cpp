#include "Filter.h"
#include <cstring>
#include <iostream>

precipitation_subscription** filter(precipitation_subscription* array[], int size, bool (*check)(precipitation_subscription* element), int& result_size)
{
	precipitation_subscription** result = new precipitation_subscription * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_precipitation_rain(precipitation_subscription* element)
{
	return strcmp(element->precipitation_name, "�����") == 0;		
}

bool check_precipitation_rainfall(precipitation_subscription* element)
{
	return element->rainfall < 1.5;
}